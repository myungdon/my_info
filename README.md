# 이력서
이력서 파일

## - 소개 -
<img src="./images/md.png" width="200" height="200"/>

* 이름: 주명돈
* 군필여부: 육군 1사단
* 취미: 음악, 드라마, 영화, 애니 감상
* Email: politia444@gmail.com
* GitLab: https://gitlab.com/myungdon
```
다양한 사람들과 협력하고 프로젝트를 진행하며 배움의 즐거움을 느끼고 있습니다.
```
## - Skills -
![skills](./images/IMG_3335.PNG)
## - 경력 -
## - 대외활동 -
* 직업 훈련(2023.09 ~ 2024.05)
## - 학력 -
* 양평 고등학교(2007 졸업)
* 강릉 대학교 산업경영학과(2012 중퇴)
## - 프로젝트 이력 -
<img src="./images/travelmeeting.png" width="100" height="80"/>

### 여행미팅
* 소개: 서로를 모르는 남녀들을 매칭하여 여행 하는 프로그램
* 담당: 프론트 앱
* 기간: 2023.12 ~ 2024.02

 <img src="./images/matddak_symbol.png" width="100" height="80"/>

### 맛딱드림
* 소개: 기존 라이더 어플의 UI를 개선 하고 좀 더 쉽게 작동할 수 있도록 만든 프로그램
* 담당: PM, 백엔드
* 기간: 2024.03 ~ 2024.05
***
